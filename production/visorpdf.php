<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//se reciben 2 parametros obligatorios codificados en base64, luego se decodifican
//se recibe parametro 'path' en formato ../files/
$path = isset($_REQUEST["path"]) ? trim($_REQUEST["path"]) : '';
//se recibe en la parametro 'file' el archivo pdf que se quiere visualizar en formato file.pdf
$file = isset($_REQUEST["file"]) ? trim($_REQUEST["file"]) : '';

$path = base64_decode($path) . '';
$file = base64_decode($file) . '';

$pathFile = $path . $file;

//se recibe 1 parametro opcional que es el zoom, sirve para la vista inicial
//en un dispositivo mobile o desktop, parametro no esta codificado en base64
//se recibe parametro 'zoom' en formato 1.25 por ejemplo. Por defecto 0.75 para mobile.
$zoom = isset($_REQUEST["zoom"]) ? trim($_REQUEST["zoom"]) : '0.75';

if (!file_exists($pathFile)) {
	echo '<h1>EL DOCUMENTO QUE INTENTA VISUALIZAR NO EXISTE EN EL SERVIDOR.</h1>';
	exit();
}
?>
<!doctype html>
 
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
  <title>My PDF Viewer</title>

  <style>
      #canvas_container {
          width: 97wh;
          /*width: 800px;*/
          height: 78vh;
          /*height: 450px;*/
          overflow: auto;
          background: #333;
          text-align: center;
          border: solid 3px;
      }
  </style>
</head>
<body>
     <div id="my_pdf_viewer">
          <div id="canvas_container">
              <canvas id="pdf_renderer"></canvas>
          </div>

          <div id="navigation_controls">
              <button id="go_previous">Anterior</button>
              <input id="current_page" value="1" type="number" style="width: 50px; text-align: center;" /> / <span id="numPages"></span>
              <button id="go_next">Siguiente</button>
              <a href="<?php echo $pathFile .'?'.mt_rand(); ?>" style="font-family: Arial, Verdana" download>Descargar</a>
          </div>

          <div id="zoom_controls">  
              <button id="zoom_in">+</button>
              <button id="zoom_out">-</button>
          </div>
     </div>

     <!-- <script src="../public/build/js/pdf.min.js"> -->
     <script src="../public/build/js/pdf.js"></script>
     <script src="../public/build/js/pdf.worker.js"></script>

     <script>
         var myState = {
             pdf: null,
             currentPage: 1,
             zoom: <?php echo $zoom; ?>,
             // zoom: 0.75,
             numPages: 0
         }

         pdfjsLib.getDocument('<?php echo $pathFile.'?'.mt_rand(); ?>').then((pdf) => {
         	myState.pdf = pdf;
         	myState.numPages = pdf.numPages;
         	document.getElementById("numPages").textContent = myState.numPages;
         	render();
         });

         function render() {
         	 // alert(myState.numPages + '|' + myState.currentPage);
         	 // if (myState.currentPage >= 1 && myState.currentPage <= myState.numPages)
         	 // {
	             myState.pdf.getPage(myState.currentPage).then((page) => {
	             	var canvas = document.getElementById("pdf_renderer");
	             	var ctx = canvas.getContext('2d');
	             	var viewport = page.getViewport(myState.zoom);
	             	canvas.width = viewport.width;
	             	canvas.height = viewport.height;

	             	page.render({
	             	    canvasContext: ctx,
	             	    viewport: viewport
	             	});
	             });
	         }
	         /*else
	         	myState.currentPage = myState.numPages;*/
         // }

         document.getElementById('go_previous')
                 .addEventListener('click', (e) => {
                     if(myState.pdf == null
                        || myState.currentPage == 1) return;
                     myState.currentPage -= 1;
                     document.getElementById("current_page")
                             .value = myState.currentPage;
                     render();
                 });

         document.getElementById('go_next')
                 .addEventListener('click', (e) => {
                 	 // alert(myState.numPages + '|' + (myState.currentPage + 1));
                     if(myState.pdf == null 
                        || (myState.currentPage + 1) > myState.numPages)
                        // || myState.currentPage > myState.pdf
                                                        // ._pdfInfo.numPages) 
                        return;
                 
                     myState.currentPage += 1;
                     document.getElementById("current_page")
                             .value = myState.currentPage;
                     render();
                 });

         document.getElementById('current_page')
                 .addEventListener('keypress', (e) => {
                     if(myState.pdf == null) return;
                 
                     // Get key code
                     var code = (e.keyCode ? e.keyCode : e.which);
                 
                     // If key code matches that of the Enter key
                     if(code == 13) {
                         var desiredPage = 
                                 document.getElementById('current_page')
                                         .valueAsNumber;
                                         
                         if(desiredPage >= 1 
                            && desiredPage <= myState.pdf
                                                     ._pdfInfo.numPages) {
                                 myState.currentPage = desiredPage;
                                 document.getElementById("current_page")
                                         .value = desiredPage;
                                 render();
                         }
                     }
                 });

         document.getElementById('zoom_in')
                 .addEventListener('click', (e) => {
                     if(myState.pdf == null) return;
                     myState.zoom += 0.25;
                     render();
                 });

         document.getElementById('zoom_out')
                 .addEventListener('click', (e) => {
                     if(myState.pdf == null) return;
                     myState.zoom -= 0.25;
                     render();
                 });
      
         // more code here
     </script>
</body>
</html>