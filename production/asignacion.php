<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

  if ($_SESSION['administrador'] == 1) {

?>
 <style>
   textarea:focus,
   input[type="text"]:focus,
   input[type="password"]:focus,
   input[type="datetime"]:focus,
   input[type="datetime-local"]:focus,
   input[type="date"]:focus,
   input[type="month"]:focus,
   input[type="time"]:focus,
   input[type="week"]:focus,
   input[type="number"]:focus,
   input[type="email"]:focus,
   input[type="url"]:focus,
   input[type="search"]:focus,
   input[type="tel"]:focus,
   input[type="color"]:focus,
   .uneditable-input:focus {   
     border-color: rgba(126, 239, 104, 0.8);
     box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset, 0 0 8px rgba(126, 239, 104, 0.6);
     outline: 0 none;
   }

   .form-control:focus {
     outline: 2px solid #1ABB9C;
   }
 </style>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Asignaciones</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-tooltip="tooltip" title="Operaciones" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a id="op_agregar" onclick="mostarform(true)">AGREGAR</a>
                          </li>
                          <li><a id="op_listar" onclick="mostarform(false)">LISTAR</a>
                          </li>
                        </ul>
                      </li>                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="listadoasignacion" class="x_content">

                    <table id="tblasignacion" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Empleado</th>                    
                          <th>RUT</th>
                          <th>Movil</th>
                          <th>Imei</th>
                          <th>Numero</th>
                          <th>Fecha</th>
                          <th>Tipo de Asignaci&oacute;n</th>
                          <th>Anexo Contrato</th>
                          <th>Acta Entrega Manual</th>
                          <th>Acta Entrega Digital</th>
                          <th>Acta Recepcion</th>
                          <th>Acta Recepcion Digital</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>

                  <div id="formularioasignacion" class="x_content">
                    <br />

                    <div class="col-md-12 center-margin">
                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <label>MOVIL</label>
                        <input type="hidden" id="idasignacion" name="idasignacion" class="form-control">
                        <select class="form-control selectpicker" data-live-search="true" id="idequipo" name="idequipo" required="required"></select>
                      </div>
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <label>EMPLEADO</label>
                          <select class="form-control selectpicker" data-live-search="true" id="idempleado" name="idempleado" required="required"></select>
                            </div>                     
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <label>TARJETA SIM</label>
                          <select class="form-control selectpicker" data-live-search="true" id="idchip" name="idchip" required="required"></select>
                      </div>              
                      <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                        <label>FECHA</label>
                                <div class='input-group date' id='myDatepicker2'>
                                    <input type='text' id="fecha" name="fecha" class="form-control" placeholder="Fecha Asignacion" />
                                    <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                      </div>
                              <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                        <label>TIPO DE ASIGNACION</label>                       
                              <select class="form-control selectpicker"  id="tasignacion" name="tasignacion" required="required">
                        <option value="" selected="selected" disabled="disabled">
                          <--SELECCIONE UNA OPCION-->
                        </option>
                                  <option value="0">NUEVA ASIGNACION</option>
                                  <option value="1">REPOSICION</option>
                              </select>
                      </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label>TIPO DE ENTREGA</label>
                      <select class="form-control selectpicker" id="tentrega" name="tentrega" required="required">
                        <option value="" selected="selected" disabled="disabled">
                          <--SELECCIONE UNA OPCION-->
                        </option>
                        <option value="0">PRESENCIAL</option>
                        <option value="1">A DISTANCIA</option>
                      </select>
                    </div>
                      <div class="clearfix"></div>
                      <div class="ln_solid"></div> 
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                          <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                          <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  </div>

                  <!-- formulario para captar la firma electronica del empleado
                  y poder firmar el acta de entrega. firma se guardara en una tabla -->
                  <div id="formulariofirdig">
                    <br />
                    <div class="">
                      <form class="form-horizontal form-label-left" id="formulariofirmadigital" name="formulariofirmadigital">
                        <input type="hidden" id="idasignacion2" name="idasignacion2" class="form-control">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <h3><b>Acta de Entrega Digital de Teléfono Móvil</b></h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b>FECHA ASIGNACIÓN: </b><span id="fechacrea">S/I</span></label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b><u>EMPLEADO</u></b></label>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                          <label>Nombre Empleado</label>
                          <p class="form-control-static" id="nomempleado">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                          <label>Rut</label>
                          <p class="form-control-static" id="rutempleado">S/I</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b><u>EQUIPO</u></b></label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Tipo</label>
                          <p class="form-control-static" id="tipoequipo">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Marca</label>
                          <p class="form-control-static" id="marcaequipo">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Modelo</label>
                          <p class="form-control-static" id="modeloequipo">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Color</label>
                          <p class="form-control-static" id="colorequipo">S/I</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b><u>TARJETA SIM</u></b></label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Operador</label>
                          <p class="form-control-static" id="operadorsim">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Número</label>
                          <p class="form-control-static" id="numerosim">S/I</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b>VALORIZACIÓN: </b><span id="valorizacion">S/I</span></label>
                        </div>

                        <div id="formfirma" name="formfirma">
                          <div class="col-md-12 col-sm-12 col-xs-12 form-group" required="Campo requerido">
                            <label>Firma</label>
                            <div class="input-group">
                              <input type="hidden" name="firma" id="firma">
                              <input type="text" disabled="disabled" class="form-control" name="firmavali" id="firmavali" required="Campo requerido">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opciones <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                  <li><a onclick="fijarfirma()">Validar</a></li>
                                  <li class="divider"></li>
                                  <li><a onclick="borrarfirma()">Borrar firma</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="firmapad" name="firmapad" >
                            <canvas id="firmafi2" class="firmafi" style="border: 2px dashed #888; width: 100%;"></canvas>
                          </div>
                          <div class="clearfix"></div>
                          <div class="ln_solid"></div> 
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                              <button class="btn btn-success" type="button" id="btnAceptar" onclick="guardaryfirmar()">Aceptar y firmar Acta</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                  <!-- formulario para captar la firma electronica del empleado
                  y poder firmar el acta de devolucion. firma se guardara en una tabla -->
                  <div id="formdevolucionfirdig">
                    <br />
                    <div class="">
                      <form class="form-horizontal form-label-left" id="formulariodevolucion" name="formulariodevolucion">
                        <input type="hidden" id="idasignacion3" name="idasignacion2" class="form-control">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <h3><b>Acta de Devolución Digital de Teléfono Móvil</b></h3>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b>FECHA DEVOLUCION: </b><span id="fechadev">S/I</span></label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b><u>EMPLEADO</u></b></label>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                          <label>Nombre Empleado</label>
                          <p class="form-control-static" id="nomempleado2">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 form-group">
                          <label>Rut</label>
                          <p class="form-control-static" id="rutempleado2">S/I</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b><u>EQUIPO</u></b></label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Tipo</label>
                          <p class="form-control-static" id="tipoequipo2">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Marca</label>
                          <p class="form-control-static" id="marcaequipo2">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Modelo</label>
                          <p class="form-control-static" id="modeloequipo2">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Color</label>
                          <p class="form-control-static" id="colorequipo2">S/I</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b><u>TARJETA SIM</u></b></label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Operador</label>
                          <p class="form-control-static" id="operadorsim2">S/I</p>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 form-group">
                          <label>Número</label>
                          <p class="form-control-static" id="numerosim2">S/I</p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b>MOTIVO: </b></label>
                          <p>
                            <textarea name="detalle" id="detalle" class="form-control" rows="3" style="width: 100%;" required></textarea>
                          </p>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label><b>SE DEJA CONSTANCIA QUE NO EXISTEN, CON ESTE DEPARTAMENTO, CARGOS PENDIENTES CON EL SR(A):</b> <span class="form-control-static" id="nomempleado3">S/I</span></label>
                          <!-- <p class="form-control-static" id="operadorsim">S/I</p> -->
                        </div>

                        <div id="formfirma2" name="formfirma">
                          <div class="col-md-12 col-sm-12 col-xs-12 form-group" required="Campo requerido">
                            <label>Firma</label>
                            <div class="input-group">
                              <input type="hidden" name="firma" id="firma2">
                              <input type="text" disabled="disabled" class="form-control" name="firmavali" id="firmavali2" required="Campo requerido">
                              <div class="input-group-btn">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opciones <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                  <li><a onclick="fijarfirma2()">Validar</a></li>
                                  <li class="divider"></li>
                                  <li><a onclick="borrarfirma2()">Borrar firma</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="firmapad2" name="firmapad" >
                            <canvas id="firmafi3" class="firmafi" style="border: 2px dashed #888; width: 100%;"></canvas>
                          </div>
                          <div class="clearfix"></div>
                          <div class="ln_solid"></div> 
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                              <button class="btn btn-success" type="button" id="btnAceptar" onclick="guardaryfirmar2()">Aceptar y firmar Acta</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                  <div class="modal fade" id="modalPreview" role="dialog">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content" id="contenido">
                              
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php
  } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
?>
<script>
    $('#myDatepicker2').datetimepicker({
        format: 'DD-MM-YYYY'
    });
</script>

<script src="../public/build/js/libs/png_support/png.js"></script>
<script src="../public/build/js/libs/png_support/zlib.js"></script>
<!-- se reemplazo jspdf.min.js por jspdf.debug.js debido a que la APP en un telefono movil real (no emulador), no se generaba el pdf en el server -->
<script src="../public/build/js/jspdf.debug.js"></script>
<script src="../public/build/js/jspdf.plugin.autotable.js"></script>
<script src="../public/build/js/jsPDFcenter.js"></script>

<script src="../public/build/js/signature_pad.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.js"></script> -->
<script src="../public/build/js/pdf.js"></script>
<script src="../public/build/js/pdf.worker.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script> -->

<!-- Script no refresca a veces en los dispositivos moviles los cambios. Se cambia la forma de cargar por uno random -->
<!-- <script type="text/javascript" src="scripts/asignacion.js"></script> -->
<script id="myScript"></script>
<script>
  var url = 'scripts/asignacion.js';
  var extra = '?t=';
  var randomNum = String((Math.floor(Math.random() * 20000000000)));
  document.getElementById('myScript').src = url + extra + randomNum;
</script>


<?php
}
ob_end_flush();
?>