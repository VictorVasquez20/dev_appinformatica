var tabla;

function init(){
    
	mostarform(false);
        
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	});
        
    $.post("../ajax/marcave.php?op=selectmarcave", function(r){
		$("#idmarca").html(r);
		$("#idmarca").selectpicker('refresh');
	});
}


function limpiar(){
	$("#idmodelove").val("");
	$("#nombre").val("");
        $("#idmarca").val("");
	$("#idmarca").selectpicker('refresh');
}

function mostarform(flag){
    
	limpiar();
        
	if(flag){
		$("#listadomodelovehiculo").hide();
		$("#formulariomodelovehiculo").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadomodelovehiculo").show();
		$("#formulariomodelovehiculo").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}
}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblmodelovehiculo').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'excelHtml5',			
			'pdf'
		],
		"ajax":{
			url:'../ajax/modelove.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
    
    e.preventDefault();
             
                    $.post( "../ajax/modelove.php?op=validarModelo", 
                       { "idmodelove": $("#idmodelove").val(), "idmarca": $("#idmarca").val(), "nombre": $.trim($("#nombre").val()) }, 
                       function( data ) {
                           
                            if(parseInt(data.cantidad)>0){

                                new PNotify({
                                    title: 'Error!',
                                    text: 'El modelo y la marca ya se encuentra registrado.',
                                    type: 'error',
                                    styling: 'bootstrap3'
                                });

                            }else{

                                $("#btnGuardar").prop("disabled", true);
                                
                                var formData = new FormData($("#formulario")[0]);
                                $.ajax({
                                        url:'../ajax/modelove.php?op=guardaryeditar',
                                        type:"POST",
                                        data:formData,
                                        contentType: false,
                                        processData:false,

                                        success: function(datos){
                                                bootbox.alert(datos);
                                                mostarform(false);
                                                tabla.ajax.reload();
                                        }
                                });
                                
                                limpiar(); 
                                
                            }                             
                        }, "json");                        

}

function mostrar(idmodelove){
	$.post("../ajax/modelove.php?op=mostrar",{idmodelove:idmodelove}, function(data,status){
		data = JSON.parse(data);
                mostarform(true);
		$("#idmodelove").val(data.idmodelove);
		$("#nombre").val(data.nombre);
                $("#idmarca").val(data.idmarca);
                $("#idmarca").selectpicker('refresh');
	});
}

function desactivar(idmodelove){
	bootbox.confirm("SEGURO QUE DESEA INHABILITAR EL MODELO DE VEHICULO?", function(result){
		if(result){
			$.post("../ajax/modelove.php?op=desactivar",{idmodelove:idmodelove}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

function activar(idmodelove){
	bootbox.confirm("SEGURO QUE DESEA HABILITAR EL MODELO DE VEHICULO?", function(result){
		if(result){
			$.post("../ajax/modelove.php?op=activar",{idmodelove:idmodelove}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

init();

