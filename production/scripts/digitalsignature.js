var tabla;
var firmaSave;
var canvas;

//funcion que se ejecuta iniciando
function init(){
	$("#btnFirmar").hide();

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#formulariofirmadigital").on("submit", function(e){
		// guardaryeditar(e);
		e.preventDefault();
		setFirma();
	})


	canvas = document.getElementById('firmafi');
	canvas.height = canvas.offsetHeight;
	canvas.width = canvas.offsetWidth;
	signaturePad = new SignaturePad(canvas, {
	backgroundColor: 'rgb(255, 255, 255)',
	penColor: 'rgb(0, 0, 0)'
	});

	var el = document.getElementById("t");
    canvas.addEventListener("click", firmamodificada, false);
    canvas.addEventListener("touchstart", firmamodificada, false);
    canvas.addEventListener("touchend", firmamodificada, false);

	firmaSave = '';
	$.post("../ajax/usuario.php?op=getFirma", function(data){
		data = JSON.parse(data);
		firmaSave = $.trim(data.firma) + '';
		if (firmaSave != null && firmaSave != '') {
			console.log('CARGOOOO');
			signaturePad.fromDataURL(firmaSave, { width: canvas.offsetWidth, height: canvas.offsetHeight});
            $("#firma").val(firmaSave);
            $("#firmavali").val("Firma cargada");
            $("#firmavali").addClass(' border border-success');
		}
	});
}

function firmamodificada(){
	console.log('mouse');

    fijarfirma();
}

function cancelarform(){
}

function firmarform(){
	var formData = new FormData($("#formulariofirmadigital")[0]);
	$.ajax({
		url:'../ajax/asignacion.php?op=firmar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
		}
	});
}


function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/asignacion.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idasignacion){
	$.post("../ajax/asignacion.php?op=mostar",{idasignacion:idasignacion}, function(data,status){
		data = JSON.parse(data);
		mostarform(true);
	
		$("#idasignacion").val(data.idasignacion);
		$("#idequipo").val(data.idequipo);
		$("#idequipo").selectpicker('refresh');
		$("#idempleado").val(data.idempleado);
		$("#idempleado").selectpicker('refresh');	
		$("#idchip").val(data.idchip);
		$("#idchip").selectpicker('refresh');

	})
}

function desactivar(idasignacion, idequipo, idchip){

	bootbox.confirm("Esta seguro que quiere inhabilitar la asignacion?", function(result){
          if(result){  
                        bootbox.prompt({
                            title: "Seleccione el motivo de la devolución",
                            inputType: 'select',
                            inputOptions: [
                                {
                                    text: 'Inhabilitar por Devolución',
                                    value: '0',
                                },
                                {
                                    text: 'Inhabilitar por teléfono Descompuesto',
                                    value: '2',
                                },
                                {
                                    text: 'Inhabilitar por Robo',
                                    value: '3',
                                }
                            ],
                            callback: function (result) {
                                if (result !== null) {
                                    $.post("../ajax/asignacion.php?op=desactivar",{idasignacion:idasignacion,idequipo:idequipo, idchip:idchip, condicion: result }, function(e){
                                        bootbox.alert(e);
                                        tabla.ajax.reload();
                                    })
                                }
                            }
                        });
                } 
	});
}


function checkcontrato(idasignacion){

	bootbox.confirm("Esta seguro que quiere marcar el contrato como entregado?", function(result){
		if(result){
			$.post("../ajax/asignacion.php?op=checkcontrato",{idasignacion:idasignacion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function checkacta(idasignacion){

	bootbox.confirm("Esta seguro que quiere marcar el acta de entrega como entregada?", function(result){
		if(result){
			$.post("../ajax/asignacion.php?op=checkacta",{idasignacion:idasignacion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function checkdev(idasignacion){

	bootbox.confirm("Esta seguro que quiere marcar el acta de devolucion como entregada?", function(result){
		if(result){
			$.post("../ajax/asignacion.php?op=checkdevolucion",{idasignacion:idasignacion}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			})	
		}
	})
}

function fijarfirma(){
            if(!signaturePad.isEmpty()){
                const padfirma = signaturePad.toDataURL();
                if(padfirma){
	            	console.log('firma fijada');
	                $("#firmavali").val("Firma modificada");
	                // $("#firmavali").val("Firma validada");
	                $("#firmavali").addClass(' border border-success');
	                $("#firma").val(padfirma);
                }else{
                    $("#firmavali").val("Error al validar");
                    $("#firmavali").addClass(' border border-danger');
                }     
            }
                  
}


function borrarfirma(){
          signaturePad.clear();
          $("#firma").val('');
          $("#firmapad").show();
          $("#firmavali").val('');
          $("#btnFirmar").hide();
}

function resetearfirma(){
		  fijarfirma();
          signaturePad.clear();
          signaturePad.fromDataURL(firmaSave, { width: canvas.offsetWidth, height: canvas.offsetHeight});
          $("#firma").val(firmaSave);
          $("#firmapad").show();
          $("#firmavali").val('Firma cargada');
          $("#btnFirmar").hide();
}

function firmadig(idasignacion) {
	// alert('width:' + window.screen.width + '|height:'  + window.screen.height);
	$.post("../ajax/asignacion.php?op=mostrarmasinfo",{idasignacion:idasignacion}, function(data,status){
		data = JSON.parse(data);
	
		$("#idasignacion2").val(idasignacion);
		$("#fechacrea").text(data.created_time);
		$("#nomempleado").text(data.nombre);
		$("#rutempleado").text(data.num_documento);
		$("#tipoequipo").text(data.tipo);
		$("#marcaequipo").text(data.marca);
		$("#modeloequipo").text(data.equipo);
		$("#colorequipo").text(data.color);
		$("#operadorsim").text(data.operador);
		$("#numerosim").text(data.numero);
		if (data.precio)
			$("#valorizacion").text('$ ' + data.precio);

	})
}

function setFirma(){
	var formData = new FormData($("#formulariofirmadigital")[0]);
	$.ajax({
		url:'../ajax/usuario.php?op=setFirma',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			var mensaje = (datos == 1) ? '<span style="color:green; font-weight: bold">Firma actualizada correctamente</span>' : '<span style="color:red">Firma esta vacía. Favor registre su firma.</span>';
			if (mensaje)
				bootbox.alert(mensaje);
		}
	});
}


init();