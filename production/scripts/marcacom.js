var tabla;

function init(){
    
	mostarform(false);
        
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	});
        
            $("#nombre").focusout(function () {
                
                if($.trim($("#nombre").val()).length>0){
                    
                    $.post( "../ajax/marcacom.php?op=validarNombre", 
                       { "idmarcacom": $("#idmarcacom").val(), "nombre": $.trim($("#nombre").val()) }, 
                       function( data ) {
                           
                            if(parseInt(data.cantidad)==0){

                                 $("#btnGuardar").prop("disabled", false);

                                new PNotify({
                                    title: 'Correcto!',
                                    text: 'El nombre es válido.',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });

                            }else{

                                $("#btnGuardar").prop("disabled", true);

                                new PNotify({
                                    title: 'Error!',
                                    text: 'El nombre ya se encuentra registrado.',
                                    type: 'error',
                                    styling: 'bootstrap3'
                                });
                            }
                        }, "json");
                        
                }
                
            });
            
}


function limpiar(){
	$("#idmarcacom").val("");
	$("#nombre").val("");
}

function mostarform(flag){
    
	limpiar();
        
	if(flag){
		$("#listadomarcacom").hide();
		$("#formulariomarcacom").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadomarcacom").show();
		$("#formulariomarcacom").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}
}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tblmarcacom').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'excelHtml5',			
			'pdf'
		],
		"ajax":{
			url:'../ajax/marcacom.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/marcacom.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostrar(idmarcacom){
	$.post("../ajax/marcacom.php?op=mostrar",{idmarcacom:idmarcacom}, function(data,status){
		data = JSON.parse(data);
                mostarform(true);
		$("#idmarcacom").val(data.idmarcacom);
		$("#nombre").val(data.nombre);
               
	});
}

function desactivar(idmarcacom){
	bootbox.confirm("SEGURO QUE DESEA INHABILITAR LA MARCA DE COMPUTADOR?", function(result){
		if(result){
			$.post("../ajax/marcacom.php?op=desactivar",{idmarcacom:idmarcacom}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

function activar(idmarcacom){
	bootbox.confirm("SEGURO QUE DESEA HABILITAR LA MARCA DE COMPUTADOR?", function(result){
		if(result){
			$.post("../ajax/marcacom.php?op=activar",{idmarcacom:idmarcacom}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

init();

