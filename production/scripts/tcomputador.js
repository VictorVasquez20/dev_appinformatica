var tabla;

function init(){
    
	mostarform(false);
        
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	});
        
            $("#nombre").focusout(function () {
                
                if($.trim($("#nombre").val()).length>0){
                    
                    $.post( "../ajax/tcomputador.php?op=validarNombre", 
                       { "idtcomputador": $("#idtcomputador").val(), "nombre": $.trim($("#nombre").val()) }, 
                       function( data ) {
                           
                            if(parseInt(data.cantidad)==0){

                                 $("#btnGuardar").prop("disabled", false);

                                new PNotify({
                                    title: 'Correcto!',
                                    text: 'El nombre es válido.',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });

                            }else{

                                $("#btnGuardar").prop("disabled", true);

                                new PNotify({
                                    title: 'Error!',
                                    text: 'El nombre ya se encuentra registrado.',
                                    type: 'error',
                                    styling: 'bootstrap3'
                                });
                            }
                        }, "json");
                        
                }
                
            });
            
}


function limpiar(){
	$("#idtcomputador").val("");
	$("#nombre").val("");
}

function mostarform(flag){
    
	limpiar();
        
	if(flag){
		$("#listadotipocomputador").hide();
		$("#formulariotipocomputador").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadotipocomputador").show();
		$("#formulariotipocomputador").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}
}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tbltipocomputador').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'excelHtml5',			
			'pdf'
		],
		"ajax":{
			url:'../ajax/tcomputador.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/tcomputador.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostrar(idtcomputador){
	$.post("../ajax/tcomputador.php?op=mostrar",{idtcomputador:idtcomputador}, function(data,status){
		data = JSON.parse(data);
                mostarform(true);
		$("#idtcomputador").val(data.idtcomputador);
		$("#nombre").val(data.nombre);
               
	});
}

function desactivar(idtcomputador){
	bootbox.confirm("SEGURO QUE DESEA INHABILITAR EL TIPO DE COMPUTADOR?", function(result){
		if(result){
			$.post("../ajax/tcomputador.php?op=desactivar",{idtcomputador:idtcomputador}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

function activar(idtcomputador){
	bootbox.confirm("SEGURO QUE DESEA HABILITAR EL TIPO DE COMPUTADOR?", function(result){
		if(result){
			$.post("../ajax/tcomputador.php?op=activar",{idtcomputador:idtcomputador}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

init();

