var tabla;

function init(){
    
	mostarform(false);
        
	listar();

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	});
        
            $("#nombre").focusout(function () {
                
                if($.trim($("#nombre").val()).length>0){
                    
                    $.post( "../ajax/operador.php?op=validarNombre", 
                       { "idoperador": $("#idoperador").val(), "nombre": $.trim($("#nombre").val()) }, 
                       function( data ) {
                           
                            if(parseInt(data.cantidad)==0){

                                 $("#btnGuardar").prop("disabled", false);

                                new PNotify({
                                    title: 'Correcto!',
                                    text: 'El nombre es válido.',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });

                            }else{

                                $("#btnGuardar").prop("disabled", true);

                                new PNotify({
                                    title: 'Error!',
                                    text: 'El nombre ya se encuentra registrado.',
                                    type: 'error',
                                    styling: 'bootstrap3'
                                });
                            }
                        }, "json");
                        
                }
                
            });
            
}


function limpiar(){
	$("#idoperador").val("");
	$("#nombre").val("");
}

function mostarform(flag){
    
	limpiar();
        
	if(flag){
		$("#listadooperador").hide();
		$("#formulariooperador").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadooperador").show();
		$("#formulariooperador").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}
}

function cancelarform(){
	limpiar();
	mostarform(false);
}

function listar(){
	tabla=$('#tbloperador').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'excelHtml5',			
			'pdf'
		],
		"ajax":{
			url:'../ajax/operador.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	$.ajax({
		url:'../ajax/operador.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostrar(idoperador){
	$.post("../ajax/operador.php?op=mostrar",{idoperador:idoperador}, function(data,status){
		data = JSON.parse(data);
                mostarform(true);
		$("#idoperador").val(data.idoperador);
		$("#nombre").val(data.nombre);
               
	});
}

function desactivar(idoperador){
	bootbox.confirm("SEGURO QUE DESEA INHABILITAR EL OPERADOR?", function(result){
		if(result){
			$.post("../ajax/operador.php?op=desactivar",{idoperador:idoperador}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

function activar(idoperador){
	bootbox.confirm("SEGURO QUE DESEA HABILITAR EL OPERADOR?", function(result){
		if(result){
			$.post("../ajax/operador.php?op=activar",{idoperador:idoperador}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}

init();

