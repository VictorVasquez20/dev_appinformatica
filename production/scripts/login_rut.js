$("#frmAccesoRut").on('submit', function(e) {
    e.preventDefault();
    rut = $("#rut").val();

    $.post("../ajax/usuario.php?op=verificarrut", { "rut": rut }, function(data) {

        if (data != "null") {
            // alert(data);
            data = JSON.parse(data);
            if (data.rut == rut) {
                $(location).attr("href", "inicio.php");
            } else {
                bootbox.alert("Su rut no tiene ninguna asignación pendiente de firma")
            }
        } else {
            bootbox.alert("Rut no registrado");
        }

    })

})