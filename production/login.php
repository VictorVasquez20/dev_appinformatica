<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Servicios e Informatica</title>

    <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />
    
    <!-- Bootstrap -->
    <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../public/build/css/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../public/build/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../public/build/css/custom.min.css" rel="stylesheet">

    <style type="text/css">
      .rut-error {
        color: #fff;
        font-weight: bold;
        background-color: red;
        padding: 2px 10px;
        display: inline-block;
        margin-left: 5px;
      }

      h5 {
         width: 100%; 
         text-align: center; 
         border-bottom: 1px solid #000; 
         line-height: 0.1em;
         margin: 10px 0 20px; 
         font-weight: bold;
      } 

      h5 span { 
          background:#F7F7F7; 
          padding:0 10px; 
      }

      .login_content h1:before {
        background:#F7F7F7; 
      }

      .login_content h1:after {
        background:#F7F7F7; 
      }
    </style>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" id="frmAcceso" name="frmAcceso">
              <h5><span>SERVICIOS E INFORMATICA</span></h5>
              <div>
                <input type="text" class="form-control" id="username" name="username" placeholder="Usuario" required="" />
              </div>
              <div>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="" />
              </div>
              <div>
                <button class="btn btn-default" type="submit">Ingresar</button>
                <a class="reset_pass" href="#">Olvidaste tu clave?</a>
              </div>

            </form>

            <div class="separator">
              <div class="clearfix"></div>
            </div>

            <form method="post" id="frmAccesoRut" class="form-horizontal form-label-left input_mask" name="frmAccesoRut">
              <h5><span>INGRESAR CON RUT</span></h5>
              <div class="form-group has-feedback">


                <input type="text" class="form-control has-feedback-left" data-inputmask="'mask' : '[*9.][999.999]-[*]'" id="rut" name="rut" placeholder="    Ingrese Rut" required="" />
                <span class="fa fa-id-card form-control-feedback left" aria-hidden="true"></span>
              </div>

              <div>
                <button class="btn btn-default" type="submit">Ingresar</button>

              </div>

            </form>
              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-building "></i>Fabrimetal</h1>
                  <p>©<?php echo date("Y"); ?> Fabrimetal sobre plantilla Bootstrap. Terminos y Privacidad</p>
                </div>
              </div>
          </section>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../public/build/js/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="../public/build/js/bootstrap.min.js"></script>
    <!-- Bootbox Alert -->
    <script src="../public/build/js/bootbox.min.js"></script>
    <script src="../public/build/js/jquery.inputmask.bundle.min.js"></script>

    <script type="text/javascript" src="scripts/login.js"></script>
    <script type="text/javascript" src="scripts/login_rut.js"></script>

    <script src="../public/build/js/custom.js"></script>

  </body>
</html>
