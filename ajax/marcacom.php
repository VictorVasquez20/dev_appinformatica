<?php 

require_once "../modelos/Marcacom.php";

$marcacom = new Marcacom();

$idmarcacom=isset($_POST["idmarcacom"])?limpiarCadena($_POST["idmarcacom"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";


switch ($_GET["op"]) {

    case 'selectmarcacom':
            $rspta = $marcacom->selectmarcacom();
            echo '<option value="" selected disabled>SELECCIONE MARCA</option>';
            while($reg = $rspta->fetch_object()){
                    echo '<option value='.$reg->idmarcacom.'>'.$reg->nombre.'</option>';
            }
    break;
                                       
    case 'guardaryeditar':

    if(empty($idmarcacom)){
            $condicion=1;
            $rspta=$marcacom->insertar($nombre,$condicion);
            echo $rspta ? "MARCA DE COMPUTADOR REGISTRADO CON EXITO" : "MARCA DE COMPUTADOR NO PUDO SER REGISTRADO";
    }
    else{
            $rspta=$marcacom->editar($idmarcacom, $nombre);
            echo $rspta ? "MARCA DE COMPUTADOR EDITADO CON EXITO" : "MARCA DE COMPUTADOR NO PUDO SER EDITADO";
    }

    break;

    case 'desactivar':

    $rspta=$marcacom->desactivar($idmarcacom);
    echo $rspta ? "MARCA DE COMPUTADOR INHABILITADO" : "MARCA DE COMPUTADOR NO SE PUEDO INHABILITAR";

    break;

    case 'activar':

    $rspta=$marcacom->activar($idmarcacom);
    echo $rspta ? "MARCA DE COMPUTADOR HABILITADO" : "MARCA DE COMPUTADOR NO SE PUDO HABILITAR";

    break;

    case 'mostrar':

    $rspta=$marcacom->mostrar($idmarcacom);

    echo json_encode($rspta);

    break;

    case 'listar':

    $rspta=$marcacom->listar();
    $data = Array();
    while ($reg = $rspta->fetch_object()){
                    $data[] = array(
                                    "0"=>($reg->condicion)?
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idmarcacom.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idmarcacom.')"><i class="fa fa-close"></i></button>':
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idmarcacom.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idmarcacom.')"><i class="fa fa-check"></i></button>',
                                    "1"=>$reg->nombre,				
                                    "2"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
                            );
    }

    $results = array(
                    "sEcho"=>1,
                    "iTotalRecords"=>count($data),
                    "iTotalDisplayRecords"=>count($data), 
                    "aaData"=>$data
                    );

    echo json_encode($results);

    break;

    case 'validarNombre':

    $rspta = $marcacom->validarNombre($idmarcacom,$nombre); 
        
    echo json_encode($rspta);

    break;    
}

 ?>