<?php 

require_once "../modelos/Tdispositivo.php";

$tdispositivo = new Tdispositivo();

$idtdispositivo=isset($_POST["idtdispositivo"])?limpiarCadena($_POST["idtdispositivo"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";

switch ($_GET["op"]) {

        case 'selecttdispositivo':

        $rspta = $tdispositivo->selecttdispositivo();
            
        echo '<option value="" selected disabled>SELECCIONE TIPO</option>';
        while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idtdispositivo.'>'.$reg->nombre.'</option>';
        }

        break;
                               
        case 'guardaryeditar':

        if(empty($idtdispositivo)){
                $condicion=1;
                $rspta=$tdispositivo->insertar($nombre,$condicion);
                echo $rspta ? "TIPO DE DISPOSITIVO REGISTRADO" : "TIPO DE DISPOSITIVO NO PUDO SER REGISTRADO";
        }
        else{
                $rspta=$tdispositivo->editar($idtdispositivo, $nombre);
                echo $rspta ? "TIPO DE DISPOSITIVO EDITADO" : "TIPO DE DISPOSITIVO NO PUDO SER EDITADO";
        }

        break;

        case 'desactivar':

        $rspta=$tdispositivo->desactivar($idtdispositivo);
        echo $rspta ? "TIPO DE COMPUTADOR INHABILITADO" : "TIPO DE COMPUTADOR NO SE PUEDO INHABILITAR";

        break;

	case 'activar':
            
	$rspta=$tdispositivo->activar($idtdispositivo);
	echo $rspta ? "TIPO DE COMPUTADOR HABILITADO" : "TIPO DE COMPUTADOR NO SE PUDO HABILITAR";
        
	break;

	case 'mostrar':
            
	$rspta=$tdispositivo->mostrar($idtdispositivo);
            
	echo json_encode($rspta);
        
	break;
			
	case 'listar':
            
	$rspta=$tdispositivo->listar();
	$data = Array();
	while ($reg = $rspta->fetch_object()){
			$data[] = array(
					"0"=>($reg->condicion)?
					'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idtdispositivo.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idtdispositivo.')"><i class="fa fa-close"></i></button>':
					'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idtdispositivo.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idtdispositivo.')"><i class="fa fa-check"></i></button>',
					"1"=>$reg->nombre,				
					"2"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
				);
	}
	
        $results = array(
			"sEcho"=>1,
			"iTotalRecords"=>count($data),
			"iTotalDisplayRecords"=>count($data), 
			"aaData"=>$data
			);

	echo json_encode($results);
        
	break;
        
        case 'validarNombre':
                    
        $rspta = $tdispositivo->validarNombre($idtdispositivo,$nombre); 
        echo json_encode($rspta);
                    
        break;
}

 ?>