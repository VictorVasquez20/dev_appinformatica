<?php 

require_once "../modelos/Marcave.php";

$marcave = new Marcave();

$idmarca=isset($_POST["idmarca"])?limpiarCadena($_POST["idmarca"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";

switch ($_GET["op"]) {

    case 'selectmarcave':
        
            $rspta = $marcave->selectmarcave();
        
            echo '<option value="" selected disabled>SELECCIONE MARCA</option>';
            while($reg = $rspta->fetch_object()){
                    echo '<option value='.$reg->idmarca.'>'.$reg->nombre.'</option>';
            }
            
    break;
                                                    
    case 'guardaryeditar':

    if(empty($idmarca)){
            $condicion=1;
            $rspta=$marcave->insertar($nombre,$condicion);
            echo $rspta ? "MARCA DE VEHICULO REGISTRADO" : "MARCA DE VEHICULO NO PUDO SER REGISTRADO";
    }
    else{
            $rspta=$marcave->editar($idmarca, $nombre);
            echo $rspta ? "MARCA DE VEHICULO EDITADO" : "TIPO DE VEHICULO NO PUDO SER EDITADO";
    }

    break;

    case 'desactivar':

    $rspta=$marcave->desactivar($idmarca);
    echo $rspta ? "MARCA DE VEHICULO INHABILITADO" : "MARCA DE VEHICULO NO SE PUEDO INHABILITAR";

    break;

    case 'activar':

    $rspta=$marcave->activar($idmarca);
    echo $rspta ? "MARCA DE VEHICULO HABILITADO" : "MARCA DE VEHICULO NO SE PUDO HABILITAR";

    break;

    case 'mostrar':

    $rspta=$marcave->mostrar($idmarca);

    echo json_encode($rspta);

    break;

    case 'listar':

    $rspta=$marcave->listar();
    $data = Array();
    while ($reg = $rspta->fetch_object()){
                    $data[] = array(
                                    "0"=>($reg->condicion)?
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idmarca.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idmarca.')"><i class="fa fa-close"></i></button>':
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idmarca.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idmarca.')"><i class="fa fa-check"></i></button>',
                                    "1"=>$reg->nombre,				
                                    "2"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
                            );
    }

    $results = array(
                    "sEcho"=>1,
                    "iTotalRecords"=>count($data),
                    "iTotalDisplayRecords"=>count($data), 
                    "aaData"=>$data
                    );

    echo json_encode($results);

    break;

    case 'validarNombre':

    $rspta = $marcave->validarNombre($idmarca,$nombre); 
        
    echo json_encode($rspta);

    break;

}

 ?>