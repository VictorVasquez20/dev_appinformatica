<?php 

require_once "../modelos/Tcomputador.php";

$tcomputador = new Tcomputador();

$idtcomputador=isset($_POST["idtcomputador"])?limpiarCadena($_POST["idtcomputador"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";

    switch ($_GET["op"]) {

        case 'selecttcomputador':

        $rspta = $tcomputador->selecttcomputador();

        echo '<option value="" selected disabled>SELECCIONE TIPO</option>';
        while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->idtcomputador.'>'.$reg->nombre.'</option>';
        }

        break;
                
        case 'guardaryeditar':

        if(empty($idtcomputador)){
                $condicion=1;
                $rspta=$tcomputador->insertar($nombre,$condicion);
                echo $rspta ? "TIPO DE COMPUTADOR REGISTRADO" : "TIPO DE COMPUTADOR NO PUDO SER REGISTRADO";
        }
        else{
                $rspta=$tcomputador->editar($idtcomputador, $nombre);
                echo $rspta ? "TIPO DE COMPUTADOR EDITADO" : "TIPO DE COMPUTADOR NO PUDO SER EDITADO";
        }

        break;

	case 'desactivar':
            
	$rspta=$tcomputador->desactivar($idtcomputador);
	echo $rspta ? "TIPO DE COMPUTADOR INHABILITADO" : "TIPO DE COMPUTADOR NO SE PUEDO INHABILITAR";
        
	break;

	case 'activar':
            
	$rspta=$tcomputador->activar($idtcomputador);
	echo $rspta ? "TIPO DE COMPUTADOR HABILITADO" : "TIPO DE COMPUTADOR NO SE PUDO HABILITAR";
        
	break;

	case 'mostrar':
            
	$rspta=$tcomputador->mostrar($idtcomputador);
            
	echo json_encode($rspta);
        
	break;
			
	case 'listar':
            
	$rspta=$tcomputador->listar();
	$data = Array();
	while ($reg = $rspta->fetch_object()){
			$data[] = array(
					"0"=>($reg->condicion)?
					'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idtcomputador.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idtcomputador.')"><i class="fa fa-close"></i></button>':
					'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idtcomputador.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idtcomputador.')"><i class="fa fa-check"></i></button>',
					"1"=>$reg->nombre,				
					"2"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
				);
	}
	
        $results = array(
			"sEcho"=>1,
			"iTotalRecords"=>count($data),
			"iTotalDisplayRecords"=>count($data), 
			"aaData"=>$data
			);

	echo json_encode($results);
        
	break;
        
        case 'validarNombre':
                    
                $rspta = $tcomputador->validarNombre($idtcomputador,$nombre); 
                echo json_encode($rspta);
                    
        break;
}

 ?>