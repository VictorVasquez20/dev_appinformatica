<?php 

require_once "../modelos/Tvehiculo.php";

$tvehiculo = new Tvehiculo();

$idtvehiculo=isset($_POST["idtvehiculo"])?limpiarCadena($_POST["idtvehiculo"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";


switch ($_GET["op"]) {

    case 'selecttvehiculo':
        
            $rspta = $tvehiculo->selecttvehiculo();
            echo '<option value="" selected disabled>SELECCIONE TIPO</option>';
            while($reg = $rspta->fetch_object()){
                    echo '<option value='.$reg->idtvehiculo.'>'.$reg->nombre.'</option>';
            }
            
    break;
                            
    case 'guardaryeditar':

    if(empty($idtvehiculo)){
            $condicion=1;
            $rspta=$tvehiculo->insertar($nombre,$condicion);
            echo $rspta ? "TIPO DE VEHICULO REGISTRADO" : "TIPO DE VEHICULO NO PUDO SER REGISTRADO";
    }
    else{
            $rspta=$tvehiculo->editar($idtvehiculo, $nombre);
            echo $rspta ? "TIPO DE VEHICULO EDITADO" : "TIPO DE VEHICULO NO PUDO SER EDITADO";
    }

    break;

    case 'desactivar':

    $rspta=$tvehiculo->desactivar($idtvehiculo);
    echo $rspta ? "TIPO DE VEHICULO INHABILITADO" : "TIPO DE VEHICULO NO SE PUEDO INHABILITAR";

    break;

    case 'activar':

    $rspta=$tvehiculo->activar($idtvehiculo);
    echo $rspta ? "TIPO DE VEHICULO HABILITADO" : "TIPO DE VEHICULO NO SE PUDO HABILITAR";

    break;

    case 'mostrar':

    $rspta=$tvehiculo->mostrar($idtvehiculo);

    echo json_encode($rspta);

    break;

    case 'listar':

    $rspta=$tvehiculo->listar();
    $data = Array();
    while ($reg = $rspta->fetch_object()){
                    $data[] = array(
                                    "0"=>($reg->condicion)?
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idtvehiculo.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idtvehiculo.')"><i class="fa fa-close"></i></button>':
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idtvehiculo.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idtvehiculo.')"><i class="fa fa-check"></i></button>',
                                    "1"=>$reg->nombre,				
                                    "2"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
                            );
    }

    $results = array(
                    "sEcho"=>1,
                    "iTotalRecords"=>count($data),
                    "iTotalDisplayRecords"=>count($data), 
                    "aaData"=>$data
                    );

    echo json_encode($results);

    break;

    case 'validarNombre':

    $rspta = $tvehiculo->validarNombre($idtvehiculo,$nombre); 
        
    echo json_encode($rspta);

    break;
}

 ?>