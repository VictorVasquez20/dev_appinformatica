<?php 

require_once "../modelos/Modelove.php";

$modelove = new Modelove();

$idmodelove=isset($_POST["idmodelove"])?limpiarCadena($_POST["idmodelove"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";
$idmarca=isset($_POST["idmarca"])?limpiarCadena(strtoupper($_POST["idmarca"])):"";

switch ($_GET["op"]) {

    case 'selectmodelove':
        
    $idmarca=$_GET["id"];
    $rspta = $modelove->selectmodelove($idmarca);
    echo '<option value="" selected disabled>SELECCIONE MODELO</option>';
    while($reg = $rspta->fetch_object()){
            echo '<option value='.$reg->idmodelove.'>'.$reg->nombre.'</option>';
    }    
    
    break;
                                                                
    case 'guardaryeditar':

    if(empty($idmodelove)){
            $condicion=1;
            $rspta=$modelove->insertar($nombre,$idmarca,$condicion);
            echo $rspta ? "MODELO DE VEHICULO REGISTRADO" : "MODELO DE VEHICULO NO PUDO SER REGISTRADO";
    }
    else{
            $rspta=$modelove->editar($idmodelove,$nombre,$idmarca);
            echo $rspta ? "MODELO DE VEHICULO EDITADO" : "MODELO DE VEHICULO NO PUDO SER EDITADO";
    }

    break;

    case 'desactivar':

    $rspta=$modelove->desactivar($idmodelove);
    echo $rspta ? "MODELO DE VEHICULO INHABILITADO" : "MODELO DE VEHICULO NO SE PUEDO INHABILITAR";

    break;

    case 'activar':

    $rspta=$modelove->activar($idmodelove);
    echo $rspta ? "MODELO DE VEHICULO HABILITADO" : "MODELO DE VEHICULO NO SE PUDO HABILITAR";

    break;

    case 'mostrar':

    $rspta=$modelove->mostrar($idmodelove);

    echo json_encode($rspta);

    break;

    case 'listar':

    $rspta=$modelove->listar();
    $data = Array();
    while ($reg = $rspta->fetch_object()){
                    $data[] = array(
                                    "0"=>($reg->condicion)?
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idmodelove.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idmodelove.')"><i class="fa fa-close"></i></button>':
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idmodelove.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idmodelove.')"><i class="fa fa-check"></i></button>',
                                    "1"=>$reg->nombre,	
                                    "2"=>$reg->nombre_marca,	
                                    "3"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
                            );
    }

    $results = array(
                    "sEcho"=>1,
                    "iTotalRecords"=>count($data),
                    "iTotalDisplayRecords"=>count($data), 
                    "aaData"=>$data
                    );

    echo json_encode($results);

    break;
    
    case 'validarModelo':

    $rspta = $modelove->validarModelo($idmodelove,$idmarca,$nombre); 
        
    echo json_encode($rspta);

    break;
}

 ?>