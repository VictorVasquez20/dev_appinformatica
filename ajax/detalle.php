<?php 

require_once "../modelos/Detalle.php";

$detalle = new Detalle();

$iddetalle=isset($_POST["iddetalle"])?limpiarCadena($_POST["iddetalle"]):"";
$marca=isset($_POST["marca"])?limpiarCadena(strtoupper($_POST["marca"])):"";
$modelo=isset($_POST["modelo"])?limpiarCadena(strtoupper($_POST["modelo"])):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";
$precio=isset($_POST["precio"])?limpiarCadena(strtoupper($_POST["precio"])):"";
$color=isset($_POST["color"])?limpiarCadena(strtoupper($_POST["color"])):"";
$tipo=isset($_POST["tipo"])?limpiarCadena(strtoupper($_POST["tipo"])):"";


switch ($_GET["op"]) {
    
        case 'selectDetalle':
            
            $rspta = $detalle->selectDetalle();
            echo '<option value="" selected disabled>SELECCIONE DETALLE</option>';
            while($reg = $rspta->fetch_object()){
                    echo '<option value='.$reg->iddetalle.'>'.$reg->marca.' '.$reg->nombre.'</option>';
            }
                        
	break;
        
                               
        case 'guardaryeditar':

        if(empty($iddetalle)){
                $rspta=$detalle->insertar($marca,$modelo,$nombre,$precio,$color,$tipo);
                echo $rspta ? "TIPO DE MOVIL REGISTRADO" : "TIPO DE MOVIL NO PUDO SER REGISTRADO";
        }
        else{
                $rspta=$detalle->editar($iddetalle,$marca,$modelo,$nombre,$precio,$color,$tipo);
                echo $rspta ? "TIPO DE MOVIL EDITADO" : "TIPO DE MOVIL NO PUDO SER EDITADO";
        }

        break;

	case 'mostrar':
            
	$rspta=$detalle->mostrar($iddetalle);
            
	echo json_encode($rspta);
        
	break;
			
	case 'listar':
            
	$rspta=$detalle->listar();
	$data = Array();
	while ($reg = $rspta->fetch_object()){
			$data[] = array(
					"0"=>'<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->iddetalle.')"><i class="fa fa-pencil"></i></button>',
                                        "1"=>$reg->marca,				
                                        "2"=>$reg->modelo,
                                        "3"=>$reg->nombre,
                                        "4"=>$reg->precio,
                                        "5"=>$reg->color,					
                                        "6"=>$reg->tipo,
				);
	}
	
        $results = array(
			"sEcho"=>1,
			"iTotalRecords"=>count($data),
			"iTotalDisplayRecords"=>count($data), 
			"aaData"=>$data
			);

	echo json_encode($results);
        
	break;

        
}

 ?>