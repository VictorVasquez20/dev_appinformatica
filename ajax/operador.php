<?php 

require_once "../modelos/Operador.php";

$operador = new Operador();

$idoperador=isset($_POST["idoperador"])?limpiarCadena($_POST["idoperador"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena(strtoupper($_POST["nombre"])):"";


switch ($_GET["op"]) {

    case 'selectOperador':
            $rspta = $operador->selectOperador();
            echo '<option value="" selected disabled>SELECCIONE OPERADOR</option>';
            while($reg = $rspta->fetch_object()){
                    echo '<option value='.$reg->idoperador.'>'.$reg->nombre.'</option>';
            }
            break;
                        
    case 'guardaryeditar':

    if(empty($idoperador)){
            $condicion=1;
            $rspta=$operador->insertar($nombre,$condicion);
            echo $rspta ? "OPERADOR REGISTRADO CON EXITO" : "OPERADOR NO PUDO SER REGISTRADO";
    }
    else{
            $rspta=$operador->editar($idoperador, $nombre);
            echo $rspta ? "OPERADOR EDITADO CON EXITO" : "OPERADOR NO PUDO SER EDITADO";
    }

    break;

    case 'desactivar':

    $rspta=$operador->desactivar($idoperador);
    echo $rspta ? "OPERADOR INHABILITADO" : "OPERADOR NO SE PUEDO INHABILITAR";

    break;

    case 'activar':

    $rspta=$operador->activar($idoperador);
    echo $rspta ? "OPERADOR HABILITADO" : "OPERADOR NO SE PUDO HABILITAR";

    break;

    case 'mostrar':

    $rspta=$operador->mostrar($idoperador);

    echo json_encode($rspta);

    break;

    case 'listar':

    $rspta=$operador->listar();
    $data = Array();
    while ($reg = $rspta->fetch_object()){
                    $data[] = array(
                                    "0"=>($reg->condicion)?
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idoperador.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idoperador.')"><i class="fa fa-close"></i></button>':
                                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idoperador.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idoperador.')"><i class="fa fa-check"></i></button>',
                                    "1"=>$reg->nombre,				
                                    "2"=>($reg->condicion)?'<span class="label bg-green">HABILITADO</span>':'<span class="label bg-red">INHABILITADO</span>'
                            );
    }

    $results = array(
                    "sEcho"=>1,
                    "iTotalRecords"=>count($data),
                    "iTotalDisplayRecords"=>count($data), 
                    "aaData"=>$data
                    );

    echo json_encode($results);

    break;

    case 'validarNombre':

    $rspta = $operador->validarNombre($idoperador,$nombre); 
        
    echo json_encode($rspta);

    break;                        
}

 ?>