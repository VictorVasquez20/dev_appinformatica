<?php 

require "../config/conexion.php";

Class Tvehiculo{
    
    //Constructor para instancias
    public function __construct(){

    }

    public function selecttvehiculo(){
            $sql="SELECT * FROM `tvehiculo` WHERE 1"; 
            return ejecutarConsulta($sql);
    }

    public function insertar($nombre,$condicion) {
        $sql="INSERT INTO `tvehiculo`(`nombre`, `condicion`) VALUES ('$nombre',$condicion)";
        return ejecutarConsulta($sql);
    }

    public function editar($idtvehiculo,$nombre) {
        $sql="UPDATE `tvehiculo` SET `nombre`='$nombre' WHERE idtvehiculo=$idtvehiculo";
        return ejecutarConsulta($sql);
    }

    public function desactivar($idtvehiculo){
            $sql="UPDATE tvehiculo SET condicion='0' WHERE idtvehiculo='$idtvehiculo'";
            return ejecutarConsulta($sql);
    }

    public function activar($idtvehiculo){
            $sql="UPDATE tvehiculo SET condicion='1' WHERE idtvehiculo='$idtvehiculo'";
            return ejecutarConsulta($sql);
    }

    public function mostrar($idtvehiculo){
            $sql="SELECT * FROM tvehiculo WHERE idtvehiculo='$idtvehiculo'";
            return ejecutarConsultaSimpleFila($sql);
    }

    public function listar(){
            $sql="SELECT * FROM tvehiculo";
            return ejecutarConsulta($sql);
    }

    public function validarNombre($idtvehiculo,$nombre) {

        $sql="SELECT count(1) as cantidad FROM tvehiculo WHERE nombre LIKE '%$nombre%'";

        if($idtvehiculo !=""){
        $sql .= " AND idtvehiculo <> $idtvehiculo";
        }     
        
        return ejecutarConsultaSimpleFila($sql);

    }
}
?>