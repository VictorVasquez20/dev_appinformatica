<?php 

require "../config/conexion.php";

Class Operador{
            
    //Constructor para instancias
    public function __construct(){

    }

    public function selectOperador(){
            $sql="SELECT * FROM operador"; 
            return ejecutarConsulta($sql);
    }
  
    public function insertar($nombre,$condicion) {
        $sql="INSERT INTO `operador`(`nombre`, `condicion`) VALUES ('$nombre',$condicion)";
        return ejecutarConsulta($sql);
    }

    public function editar($idoperador,$nombre) {
        $sql="UPDATE `operador` SET `nombre`='$nombre' WHERE idoperador=$idoperador";
        return ejecutarConsulta($sql);
    }

    public function desactivar($idoperador){
            $sql="UPDATE operador SET condicion='0' WHERE idoperador='$idoperador'";
            return ejecutarConsulta($sql);
    }

    public function activar($idoperador){
            $sql="UPDATE operador SET condicion='1' WHERE idoperador='$idoperador'";
            return ejecutarConsulta($sql);
    }

    public function mostrar($idoperador){
            $sql="SELECT * FROM operador WHERE idoperador='$idoperador'";
            return ejecutarConsultaSimpleFila($sql);
    }

    public function listar(){
            $sql="SELECT * FROM operador";
            return ejecutarConsulta($sql);
    }

    public function validarNombre($idoperador,$nombre) {

        $sql="SELECT count(1) as cantidad FROM operador WHERE nombre LIKE '%$nombre%'";

        if($idoperador !=""){
            $sql .= " AND idoperador <> $idoperador";
        }  
        
        return ejecutarConsultaSimpleFila($sql);

    }
  
    
}
?>