<?php 

require "../config/conexion.php";

Class Detalle{
        //Constructor para instancias
        public function __construct(){

        }

        public function selectDetalle(){
                $sql="SELECT * FROM detalle"; 
                return ejecutarConsulta($sql);
        }
        
        public function insertar($marca,$modelo,$nombre,$precio,$color,$tipo) {
            $sql="INSERT INTO `detalle`(`marca`, `modelo`, `nombre`, `precio`, `color`, `tipo`) VALUES ('$marca','$modelo','$nombre',$precio,'$color','$tipo')";
            return ejecutarConsulta($sql);
        }

        public function editar($iddetalle,$marca,$modelo,$nombre,$precio,$color,$tipo) {
            $sql="UPDATE `detalle` SET `marca`='$marca',`modelo`='$modelo',`nombre`='$nombre',`precio`=$precio,`color`='$color',`tipo`='$tipo' WHERE iddetalle=$iddetalle";
            return ejecutarConsulta($sql);
        }

        public function mostrar($iddetalle){
                $sql="SELECT * FROM detalle WHERE iddetalle=$iddetalle";
                return ejecutarConsultaSimpleFila($sql);
        }

        public function listar(){
                $sql="SELECT * FROM detalle";
                return ejecutarConsulta($sql);
        }

 
        
}
?>