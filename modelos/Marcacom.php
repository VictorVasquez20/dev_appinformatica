<?php 

require "../config/conexion.php";

Class Marcacom{
    
    //Constructor para instancias
    public function __construct(){

    }

    public function selectmarcacom(){
            $sql="SELECT * FROM marcacom WHERE condicion=1"; 
            return ejecutarConsulta($sql);
    }

    public function insertar($nombre,$condicion) {
        $sql="INSERT INTO `marcacom`(`nombre`, `condicion`) VALUES ('$nombre',$condicion)";
        return ejecutarConsulta($sql);
    }

    public function editar($idmarcacom,$nombre) {
        $sql="UPDATE `marcacom` SET `nombre`='$nombre' WHERE idmarcacom=$idmarcacom";
        return ejecutarConsulta($sql);
    }

    public function desactivar($idmarcacom){
            $sql="UPDATE marcacom SET condicion='0' WHERE idmarcacom='$idmarcacom'";
            return ejecutarConsulta($sql);
    }

    public function activar($idmarcacom){
            $sql="UPDATE marcacom SET condicion='1' WHERE idmarcacom='$idmarcacom'";
            return ejecutarConsulta($sql);
    }

    public function mostrar($idmarcacom){
            $sql="SELECT * FROM marcacom WHERE idmarcacom='$idmarcacom'";
            return ejecutarConsultaSimpleFila($sql);
    }

    public function listar(){
            $sql="SELECT * FROM marcacom";
            return ejecutarConsulta($sql);
    }

    public function validarNombre($idmarcacom,$nombre) {

        $sql="SELECT count(1) as cantidad FROM marcacom WHERE nombre LIKE '%$nombre%'";

        if($idmarcacom !=""){
            $sql .= " AND idmarcacom <> $idmarcacom";
        }  
        
        return ejecutarConsultaSimpleFila($sql);

    }
}
?>