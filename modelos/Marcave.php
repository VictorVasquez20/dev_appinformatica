<?php 

require "../config/conexion.php";

Class Marcave{
    
    //Constructor para instancias
    public function __construct(){

    }

    public function selectmarcave(){
            $sql="SELECT * FROM marcave"; 
            return ejecutarConsulta($sql);
    }

    public function insertar($nombre,$condicion) {
        $sql="INSERT INTO `marcave`(`nombre`, `condicion`) VALUES ('$nombre',$condicion)";
        return ejecutarConsulta($sql);
    }

    public function editar($idmarca,$nombre) {
        $sql="UPDATE `marcave` SET `nombre`='$nombre' WHERE idmarca=$idmarca";
        return ejecutarConsulta($sql);
    }

    public function desactivar($idmarca){
            $sql="UPDATE marcave SET condicion='0' WHERE idmarca='$idmarca'";
            return ejecutarConsulta($sql);
    }

    public function activar($idmarca){
            $sql="UPDATE marcave SET condicion='1' WHERE idmarca='$idmarca'";
            return ejecutarConsulta($sql);
    }

    public function mostrar($idmarca){
            $sql="SELECT * FROM marcave WHERE idmarca='$idmarca'";
            return ejecutarConsultaSimpleFila($sql);
    }

    public function listar(){
            $sql="SELECT * FROM marcave";
            return ejecutarConsulta($sql);
    }

    public function validarNombre($idmarca,$nombre) {

        $sql="SELECT count(1) as cantidad FROM marcave WHERE nombre LIKE '%$nombre%'";

        if($idmarca !=""){
        $sql .= " AND idmarca <> $idmarca";
        }     
        
        return ejecutarConsultaSimpleFila($sql);

    }
}
?>